import { StyleSheet, StatusBar } from 'react-native'
import colors from './colors'

const mainStyles = StyleSheet.create({
  bgBlack: {
    backgroundColor: colors.black,
  },
  bgGray: {
    backgroundColor: colors.gray,
  },
  bgBlue: {
    backgroundColor: colors.blue,
  },
  textWhite: {
    color: colors.white,
  },
  textOrange: {
    color: colors.orange,
  },
  textBlue: {
    color: colors.blue,
  },
  textBlack: {
    color: colors.black,
  },
  textGray: {
    color: colors.gray,
  },
  textRed: {
    color: colors.red,
  },
  flex1: {
    flex: 1,
  },
  flex2: {
    flex: 2,
  },
  fontRationale: {
    fontFamily: 'Rationale-Alt',
  },
  textSm: {
    fontSize: 16,
  },
  textMd: {
    fontSize: 24,
  },
  textLg: {
    fontSize: 32,
  },
  textXl: {
    fontSize: 48,
  },
  textCenter: {
    textAlign: 'center',
  }
})

export default mainStyles