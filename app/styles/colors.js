const colors = { 
  orange: '#da6e0c',
  turquoise: '#258193',
  blue: '#00bcd4',
  black: '#1f2a2f',
  gray: '#33464e',
  white: '#efeded',
  red: '#ff3223',
}

export default colors