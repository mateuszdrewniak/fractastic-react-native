import Fraction from './Fraction'
import Operator from './Operator'
import OperatorArray from './OperatorArray'

class Calculator {
  static evaluate(formula) {
    let fractionString = ''
    let currentRank = 0

    let incorrectSyntax = false
    let insideOfAFraction = false
    let minusRegistered = false
    let operatorAwaitingAnArgument = false
    let lastCharIsAClosedBracket = false
    let atLeastOneFraction = false
    let fraction = null

    let operators = new OperatorArray()
    let operator = null
    let index = 0

    for(let char of formula) {
      if(insideOfAFraction) {
        if(char === ')') {
          fractionString = `(${fractionString})`
          if(!Fraction.valid(fractionString)) {
            incorrectSyntax = true
            break
          }

          fraction = new Fraction({ string: fractionString })
          if(atLeastOneFraction) operators.last().toRight = fraction

          lastCharIsAClosedBracket = true
          fractionString = ''
          insideOfAFraction = false
          operatorAwaitingAnArgument = false
          minusRegistered = false
          currentRank -= 3
          atLeastOneFraction = true
        } else {
          fractionString += char
        }
      } else if(char === '(') {
        if(lastCharIsAClosedBracket) {
          incorrectSyntax = true
          break
        }
        currentRank += 3
        if(minusRegistered) minusRegistered = false
      } else if(char === ')') {
        lastCharIsAClosedBracket = true
        if(currentRank === 0) {
          incorrectSyntax = true
          break
        }
        currentRank -= 3
      } else if(Fraction.numeric(char) || char === '.') {
        if(currentRank === 0) {
          incorrectSyntax = true
          break
        }

        if(!insideOfAFraction) {
          insideOfAFraction = true
          operatorAwaitingAnArgument = false

          if(minusRegistered) {
            fractionString += '-'
            minusRegistered = false
            operators.pop()
          }
        }
        lastCharIsAClosedBracket = false
        fractionString += char
      } else if(char === '-') {
        minusRegistered = true
        operatorAwaitingAnArgument = true
        operator = new Operator(char, this.calculateRank(currentRank, char), fraction)
        operators.push(operator)
        lastCharIsAClosedBracket = false
      } else if(this.charIsAnOperator(char)) {
        if(operatorAwaitingAnArgument || insideOfAFraction) {
          incorrectSyntax = true
          break
        }

        lastCharIsAClosedBracket = false
        operatorAwaitingAnArgument = true
        operator = new Operator(char, this.calculateRank(currentRank, char), fraction)
        operators.push(operator)
      } else if(char !== ' ') {
        incorrectSyntax = true
        break
      }
      index += 1
    }

    if(operatorAwaitingAnArgument || insideOfAFraction || incorrectSyntax) return null

    operators.sortDescending()
    for(let op of operators) {
      switch(op.operatorCharacter) {
        case '+':
          fraction = op.toLeft.add(op.toRight)
          break
        case '-':
          fraction = op.toLeft.subtract(op.toRight)
          break
        case '*':
          fraction = op.toLeft.multiply(op.toRight)
          break
        case '/':
          fraction = op.toLeft.divide(op.toRight)
          break
        case ':':
          fraction = op.toLeft.divide(op.toRight)
          break
        case '÷':
          fraction = op.toLeft.divide(op.toRight)
          break
        case '^':
          fraction = op.toLeft.power(op.toRight)
          break
      }

      let left = op.toLeft
      for(let o of operators) {
        if(o.toLeft === left) o.toLeft = fraction
        if(o.toRight === left) o.toRight = fraction
      }

      op.markAsExecuted()
      if(op !== operators.last) {
        let newOpIndex = this.findOperatorWithLeft(operators, op.toRight)
        if(newOpIndex >= 0) operators[newOpIndex].toLeft = op.toLeft
      }
    }

    return fraction
  }

  static valid(formula) {
    let fractionString = ''
    let currentRank = 0

    let incorrectSyntax = false
    let insideOfAFraction = false
    let minusRegistered = false
    let operatorAwaitingAnArgument = false
    let lastCharIsAClosedBracket = false
    let atLeastOneFraction = false
    let index = 0

    for(let char of formula) {
      if(insideOfAFraction) {
        if(char === ')') {
          fractionString = `(${fractionString})`
          if(!Fraction.valid(fractionString)) {
            incorrectSyntax = true
            break
          }

          lastCharIsAClosedBracket = true
          fractionString = ''
          insideOfAFraction = false
          operatorAwaitingAnArgument = false
          minusRegistered = false
          currentRank -= 3
          atLeastOneFraction = true
        } else {
          fractionString += char
        }
      } else if(char === '(') {
        if(lastCharIsAClosedBracket) {
          incorrectSyntax = true
          break
        }
        currentRank += 3
        if(minusRegistered) minusRegistered = false
      } else if(char === ')') {
        lastCharIsAClosedBracket = true
        if(currentRank === 0) {
          incorrectSyntax = true
          break
        }
        currentRank -= 3
      } else if(Fraction.numeric(char) || char === '.') {
        if(currentRank === 0) {
          incorrectSyntax = true
          break
        }

        if(!insideOfAFraction) {
          insideOfAFraction = true
          operatorAwaitingAnArgument = false

          if(minusRegistered) {
            fractionString += '-'
            minusRegistered = false
          }
        }
        lastCharIsAClosedBracket = false
        fractionString += char
      } else if(char == '-') {
        minusRegistered = true
        operatorAwaitingAnArgument = true
        lastCharIsAClosedBracket = false
      } else if(this.charIsAnOperator(char)) {
        if(operatorAwaitingAnArgument || insideOfAFraction) {
          incorrectSyntax = true
          break
        }

        lastCharIsAClosedBracket = false
        operatorAwaitingAnArgument = true
      } else if(char !== ' ') {
        incorrectSyntax = true
        break
      }
      index += 1
    }

    if(operatorAwaitingAnArgument || insideOfAFraction) {
      incorrectSyntax = true
    }

    return !incorrectSyntax
  }

  // private

  static calculateRank(currentRank, operator) {
    switch(operator) {
      case '*':
        return currentRank + 1
      case '/':
        return currentRank + 1
      case ':':
        return currentRank + 1
      case '÷':
        return currentRank + 1
      case '^':
        return currentRank + 2
      default:
        return currentRank
    }
  }

  static charIsAnOperator(char) {
    return ['+', '-', '*', '/', ':', '÷', '^'].indexOf(char) !== -1
  }

  static findOperatorWithLeft(operators, fraction) {
    let counter = 0
    for(op of operators) {
      if(op.toLeft == fraction && op.notExecuted())
        return counter

      counter += 1
    }

    return -1
  }
}

export default Calculator
