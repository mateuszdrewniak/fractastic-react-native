import IncorrectArgumentsError from './IncorrectArgumentsError'
import Fraction from './Fraction'

class Operator {
  // attr_accessor :operatorCharacter, :rank, :executed, :toLeft, :toRight

  constructor(operatorCharacter, rank, toLeft = null, toRight = null) {
    if(toLeft && (typeof(toLeft) !== 'object' || toLeft.constructor != Fraction) || toRight && (typeof(toRight) !== 'object' || toRight.constructor !== Fraction)) {
      throw new IncorrectArgumentsError()
    }

    this.operatorCharacter = operatorCharacter
    this.rank = rank
    this.toLeft = toLeft
    this.toRight = toRight
    this.executed = false
  }

  cmp(other) {
    if(typeof(other) !== 'object' || other.constructor !== Operator)
      throw new IncorrectArgumentsError

    if(this.rank == other.rank) return 0
    if(this.rank > other.rank) return 1

    return -1
  }

  gt(other) {
    if(typeof(other) !== 'object' || other.constructor !== Operator)
      throw new IncorrectArgumentsError
    
    return this.cmp(other) == 1
  }

  lt(other) {
    if(typeof(other) !== 'object' || other.constructor !== Operator)
      throw new IncorrectArgumentsError
    
    return this.cmp(other) == -1
  }

  eq(other) {
    if(typeof(other) !== 'object' || other.constructor !== Operator)
      throw new IncorrectArgumentsError
    
    return this.cmp(other) == 0
  }

  toString() {
    return `(${this.operatorCharacter}, ${this.rank})`
  }

  decreaseRank(number = 1) {
    this.rank -= number
  }

  increaseRank(number = 1) {
    this.rank += number
  }

  markAsExecuted() {
    this.executed = true
  }

  notExecuted() {
    return !this.executed
  }
}

export default Operator
