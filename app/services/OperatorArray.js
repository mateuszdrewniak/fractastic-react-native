class OperatorArray extends Array { 
  sortDescending() {
    this.sort((a, b) => b.cmp(a))
  }

  sortAscending() {
    this.sort((a, b) => a.cmp(b))
  }

  toString() {
    let string = ''
    
    for(let a of this) {
      string += a.toString()
      if(a !== this.last()) string += ', '
    }
    
    return string
  }

  last() {
    if(this.length == 0) return

    return this[this.length - 1]
  }

  first() {
    if(this.length == 0) return

    return this[0]
  }
}

export default OperatorArray
