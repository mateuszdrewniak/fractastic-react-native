class DividingByZeroError extends Error {
  constructor(message = 'Dividing By Zero!') {
    super(message)
  }
}

export default DividingByZeroError
