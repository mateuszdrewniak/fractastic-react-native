class IncorrectArgumentsError extends Error {
  constructor(message = "Incorrect Arguments!") {
    super(message)
  }
}

export default IncorrectArgumentsError
