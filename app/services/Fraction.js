import DividingByZeroError from './DividingByZeroError'
import IncorrectArgumentsError from './IncorrectArgumentsError'
import IncorrectStringSyntaxError from './IncorrectStringSyntaxError'

class Fraction {
  // attr_accessor :wholePart, :numerator, :denominator, :negative

 constructor({ wholePart = 0, numerator = 0, denominator = 1, string = '', numeric = false } = {}) {
    if(string.trim() !== '') {
      this.toZero()
      this.parseFractionString(string)
      return
    }

    if(typeof(numeric) === 'number') {
      this.convertNumber(numeric)
      return
    }

    if(denominator === 0 && numerator !== 0) throw new DividingByZeroError()

    this.wholePart = wholePart
    this.numerator = numerator
    this.denominator = denominator
    this.negative = false

    this.fixNegativity()
    this.simplify()
  }

  zero() {
    if(this.numerator === 0) return true 

    return false
  }

  integer() {
    if(this.denominator === 1 && !this.zero()) return true

    return false
  }

  one() {
    if(this.numerator === 1 && this.denominator === 1 && this.wholePart === 0) return true

    return false
  }

  simplify() {
    if(this.integer()) return true

    if(this.zero()) {
      this.denominator = 1
      this.wholePart = 0
      this.negative = false
    } else {
      let greatestCommonDivisor = this.constructor.greatestCommonDivisor(this.numerator, this.denominator)

      this.denominator /= greatestCommonDivisor
      this.numerator /= greatestCommonDivisor
    }

    this.toProperFraction()

    return true
  }

  toProperFraction() {
    if(this.zero()) return true

    if(this.denominator === 1 && this.numerator !== 1) {
      this.wholePart += this.numerator
      this.numerator = 1
    } else if(this.numerator > this.denominator) {
      let newWholePart = Math.floor(this.numerator / this.denominator)
      this.wholePart += newWholePart
      this.numerator %= this.denominator
    }

    return true
  }

  toImproper() {
    if(this.wholePart === 0) return true

    if(this.numerator == 1 && this.denominator == 1) this.numerator -= 1
    this.numerator += this.wholePart * this.denominator
    this.wholePart = 0

    return true
  }

  log() {
    console.log(this.toString())
  }

  toZero() {
    this.wholePart = 0
    this.numerator = 0
    this.denominator = 1
    this.negative = false

    return true
  }

  toFloat() {
    if(this.zero()) return 0.0

    let fraction = this.clone()
    fraction.toImproper()
    fraction.minusToNumerator()

    return fraction.numerator / fraction.denominator
  }

  clone() {
    return Object.assign(new this.constructor(), this)
  }

  toInteger() {
    if(this.zero()) return 0

    let fraction = this.clone()
    fraction.toProperFraction()

    let result = fraction.wholePart
    if(fraction.negative) result = -result

    return result
  }

  toString(improper = false) {
    let fraction = this.clone()
    if(improper) fraction.toImproper()

    let fractionString = '('
    if(this.negative) fractionString += '-'

    if(fraction.zero()) {
      fractionString += '0'
    } else if(fraction.one()) {
      fractionString += '1'
    } else {
      if(fraction.wholePart !== 0) fractionString += fraction.wholePart.toString()
      if(fraction.numerator !== 1 || fraction.denominator !== 1) {
        if(fraction.wholePart !== 0) fractionString += ' '
        fractionString += fraction.numerator.toString()
        if(!fraction.integer()) fractionString += '/' + fraction.denominator.toString()
      }
    }

    return fractionString + ')'
  }

  subtract(other) {
    this.check_argument(other)

    let fraction = this.clone()
    let object

    if(typeof(other) === 'number') {
      object = new Fraction({ numeric: other })
    } else {
      object = other.clone()
    }

    if(!object.zero()) fraction.toCommonDenominator(object)

    fraction.minusToNumerator()
    object.minusToNumerator()

    fraction.numerator -= object.numerator
    fraction.minusToNegativeField()
    fraction.simplify()

    return fraction
  }

  add(other) {
    this.check_argument(other)

    let fraction = this.clone()
    let object

    if(typeof(other) === 'number') {
      object = new Fraction({ numeric: other })
    } else {
      object = other.clone()
    }

    if(!object.zero()) fraction.toCommonDenominator(object)

    fraction.minusToNumerator()
    object.minusToNumerator()

    fraction.numerator += object.numerator
    fraction.minusToNegativeField()
    fraction.simplify()

    return fraction
  }

  multiply(other) {
    this.check_argument(other)

    let fraction = this.clone()
    let object

    if(typeof(other) === 'number') {
      object = new Fraction({ numeric: other })
    } else {
      object = other.clone()
    }

    fraction.toImproper()
    object.toImproper()

    let negative_counter = 0
    if(fraction.negative) negative_counter += 1
    if(object.negative) negative_counter += 1
    fraction.negative = negative_counter % 2 == 0 ? false : true

    fraction.numerator *= object.numerator
    fraction.denominator *= object.denominator

    fraction.simplify()


    if(fraction.zero()) fraction.wholePart = 1

    return fraction
  }

  divide(other) {
    this.check_argument(other)

    let fraction = this.clone()
    let object
    
    if(typeof(other) === 'number') {
      object = new Fraction({ numeric: other })
    } else {
      object = other.clone()
    }

    if(object.zero()) throw new DividingByZeroError()

    object.toReciprocal()
    fraction = fraction.multiply(object)

    return fraction
  }

  power(other) {
    this.check_argument(other)

    if(typeof(other) === 'number') {
      return this._power_(other)
    } else {
      let x = other.toFloat()
      return this._power_(x)
    }
  }

  static valid(string) {
    if(typeof(string) !== 'string') return false

    return this.validateFractionString(string)
  }

  static floatingPointPart(number) {
    if(typeof(number) !== 'number') throw new IncorrectArgumentsError

    let result = '0.'
    let numberString = number.toString()
    let pastDecimalSeparator = false

    for(let c of numberString) {
      if(pastDecimalSeparator)
        result += c
      else if(c === '.')
        pastDecimalSeparator = true
    }

    return parseFloat(result)
  }

  static leastCommonMultiple(first, second) {
    return (first * second) / this.greatestCommonDivisor(first, second)
  }

  static greatestCommonDivisor(first, second) {
    if(typeof(first) !== 'number' || typeof(second) !== 'number') throw new IncorrectArgumentsError

    if(second === 0) return first

    return this.greatestCommonDivisor(second, first % second)
  }

  static numeric(char) {
    return char.match(/\d/) !== null
  }

  static letter(char) {
    return char.match(/[A-Za-z]/) !== null
  }

  static validateFractionString(string) {
    let isFloat = false
    
    for(let c of string) {
      if(c == '.') {
        isFloat = true
        break
      }
    }

    if(isFloat)
      return this.validateFloatString(string)
    else
      return this.validateString(string)
  }

  static validateString(string) {
    let stage = 0
    let openBracket = false
    let incorrectSyntax = false
    let atEnd = false
    let digitPresent = false
    let minusIsFree = true

    for(let c of string) {
      if(atEnd || this.letter(c)) {
        incorrectSyntax = true
        break
      }

      if(openBracket) {
        if(stage === 0) {
          if(c === '-' && minusIsFree) {
            minusIsFree = false
          } else if(this.numeric(c)) {
            digitPresent = true
          } else if(c === ' ' && digitPresent) {
            stage = 1
            minusIsFree = true
            digitPresent = false
          } else if(c === '/' && digitPresent) {
            stage = 2
            minusIsFree = true
            digitPresent = false
          } else if(c === ')' && digitPresent) {
          } else {
            incorrectSyntax = true
            break
          }
        } else if(stage === 1) {
          if(c === '-' && minusIsFree) {
            minusIsFree = false
          } else if(this.numeric(c)) {
            digitPresent = true
          } else if(c === '/' && digitPresent) {
            stage = 2
            minusIsFree = true
            digitPresent
          } else {
            incorrectSyntax = true
            break
          }
        } else if(stage === 2) {
          if(c === '-' && minusIsFree) {
            minusIsFree = false
          } else if(this.numeric(c)) {
            digitPresent = true
          } else if(c === ')' && digitPresent) {
          } else {
            incorrectSyntax = true
            break
          }
        }
      }

      if(c === '(') {
        if(openBracket) {
          incorrectSyntax = true
          break
        }
        openBracket = true
      } else if(c === ')') {
        if(!openBracket) {
          incorrectSyntax = true
          break
        }
        openBracket = false
        atEnd = true
      }
    }

    if(openBracket) incorrectSyntax = true

    return !incorrectSyntax
  }

  static validateFloatString(string) {
    let openBracket = false
    let incorrectSyntax = false
    let atEnd = false
    let digitPresent = false
    let floatingPointPresent = false
    let minusIsFree = true

    for(let c of string) {
      if(atEnd || this.letter(c)) {
        incorrectSyntax = true
        break
      }

      if(openBracket) {
        if(c === '-') {
          if(!minusIsFree || digitPresent) {
            incorrectSyntax = true
            break
          }
          minusIsFree = false
        } else if(this.numeric(c)) {
          digitPresent = true
        } else if(c === '.') {
          if(floatingPointPresent || !digitPresent) {
            incorrectSyntax = true
            break
          }
          floatingPointPresent = true
        } else if(c === ')') {
          openBracket = false
          atEnd = true
        } else {
          incorrectSyntax = true
          break
        }
      } else if(c === '(') {
        openBracket = true
      } else {
        incorrectSyntax = true
        break
      }
    }

    if(openBracket) incorrectSyntax = true

    return !incorrectSyntax
  }

  toCommonDenominator(other) {
    this.toImproper()
    other.toImproper()

    let leastCommonMultiple = this.constructor.leastCommonMultiple(this.denominator, other.denominator)

    let numeratorMultiplication = leastCommonMultiple / this.denominator
    this.numerator *= numeratorMultiplication
    this.denominator = leastCommonMultiple

    numeratorMultiplication = leastCommonMultiple / other.denominator
    other.numerator *= numeratorMultiplication
    other.denominator = leastCommonMultiple

    return true
  }

  toReciprocal() {
    this.toImproper()
    let aux = this.denominator
    this.denominator = this.numerator
    this.numerator = aux

    return true
  }

  // protected

  minusToNumerator() {
    if(!this.negative) return true

    this.negative = false
    this.numerator = -this.numerator

    return true
  }

  minusToNegativeField() {
    if(this.numerator > 0) return true

    this.negative = true
    this.numerator = Math.abs(this.numerator)

    return true
  }

  // private

  check_argument(other) {
    if(typeof(other) === 'number' || typeof(other) === 'object' && other.constructor === Fraction)
      return

    throw new IncorrectArgumentsError()
  }

  _power_(x) {
    let fraction = this.clone()
    if(x % 2 === 0) fraction.negative = false

    if(x < 0) {
      fraction.toReciprocal()
      x = Math.abs(x)
    } else
      fraction.toImproper()

    if(Math.floor(x) === x) {
      fraction.numerator **= Math.floor(x)
      fraction.denominator **= Math.floor(x)
      fraction.simplify()
    } else {
      let float = Math.abs(fraction.toFloat())
      float **= x
      if(fraction.negative) float = -float
      fraction = new Fraction({ numeric: float })
    }

    return fraction
  }

  fixNegativity() {
    if(this.argumentsMakeNegative()) this.negative = !this.negative

    this.wholePart = Math.abs(this.wholePart)
    this.numerator = Math.abs(this.numerator)
    this.denominator = Math.abs(this.denominator)

    return true
  }

  argumentsMakeNegative() {
    let negativeAttributes = 0
    for(let a of ['wholePart', 'numerator', 'denominator']) {
      if(this[a] < 0) negativeAttributes += 1
    }

    if(negativeAttributes % 2 === 1) return true

    return false
  }

  parseFractionString(string) {
    let isFloat = false
    for(let c of string) {
      if(c === '.') {
        isFloat = true
        break
      }
    }

    if(isFloat)
      this.parseFloatString(string)
    else
      this.parseString(string)
  }

  parseString(string) {
    let stage = 0
    let numberString = ''

    let openBracket = false
    let incorrectSyntax = false
    let atEnd = false
    let digitPresent = false

    let minusIsFree = true

    for(let c of string) {
      if(atEnd || this.constructor.letter(c)) {
        incorrectSyntax = true
        break
      }

      if(openBracket) {
        if(stage === 0) {
          if(c === '-' && minusIsFree) {
            this.negative = true
            minusIsFree = false
          } else if(this.constructor.numeric(c)) {
            numberString += c
            digitPresent = true
          } else if(c === ' ' && digitPresent) {
            stage = 1
            this.wholePart = parseInt(numberString)
            numberString = ''
            minusIsFree = true
            digitPresent = false
          } else if(c === '/' && digitPresent) {
            stage = 2
            this.numerator = parseInt(numberString)
            numberString = ''
            minusIsFree = true
            digitPresent = false
          } else if(c === ')' && digitPresent) {
            this.numerator = parseInt(numberString)
            this.denominator = 1
          } else {
            incorrectSyntax = true
            break
          }
        } else if(stage === 1) {
          if(c === '-' && minusIsFree) {
            this.negative = !this.negative
            minusIsFree = false
          } else if(this.constructor.numeric(c)) {
            numberString += c
            digitPresent = true
          } else if(c === '/' && digitPresent) {
            stage = 2
            this.numerator = parseInt(numberString)
            numberString = ''
            minusIsFree = true
          } else {
            incorrectSyntax = true
            break
          }
        } else if(stage === 2) {
          if(c === '-' && minusIsFree) {
            this.negative = !this.negative
            minusIsFree = false
          } else if(this.constructor.numeric(c)) {
            numberString += c
            digitPresent = true
          } else if(c === ')' && digitPresent) {
            this.denominator = parseInt(numberString)
          } else {
            incorrectSyntax = true
            break
          }
        }
      }

      if(c === '(') {
        if(openBracket) {
          incorrectSyntax = true
          break
        }
        openBracket = true
      } else if(c === ')') {
        if(!openBracket) {
          incorrectSyntax = true
          break
        }
        openBracket = false
        atEnd = true
      }
    }

    if(incorrectSyntax || openBracket) throw new IncorrectStringSyntaxError()

    this.simplify()
  }

  parseFloatString(string) {
    let numberString = ''
    let openBracket = false
    let incorrectSyntax = false
    let atEnd = false
    let digitPresent = false
    let floatingPointPresent = false

    minusIsFree = true

    for(let c of string) {
      if(atEnd || this.constructor.letter(c)) {
        incorrectSyntax = true
        break
      }

      if(openBracket) {
        if(c === '-') {
          if(!minusIsFree || digitPresent) {
            incorrectSyntax = true
            break
          }
          minusIsFree = false
          numberString += c
        } else if(this.constructor.numeric(c)) {
          digitPresent = true
          numberString += c
        } else if(c === '.') {
          if(floatingPointPresent || !digitPresent) {
            incorrectSyntax = true
            break
          }
          floatingPointPresent = true
          numberString += c
        } else if(c === ')') {
          openBracket = false
          atEnd = true
        } else {
          incorrectSyntax = true
          break
        }
      } else if(c === '(') {
        openBracket = true
      } else {
        incorrectSyntax = true
        break
      }
    }

    if(openBracket || incorrectSyntax) throw new IncorrectStringSyntaxError()

    this.convertNumber(parseFloat(numberString))
  }

  convertNumber(number) {
    if(number === 0) {
      this.toZero()
      return
    }

    if(number < 0) {
      number = Math.abs(number)
      this.negative = true
    }

    this.wholePart = Math.floor(number)
    let newDenominator = 1
    let floatingPointPart = this.constructor.floatingPointPart(number)

    while(floatingPointPart > 0) {
      floatingPointPart *= newDenominator
      floatingPointPart = this.constructor.floatingPointPart(floatingPointPart)
      newDenominator *= 10
    }

    this.numerator = this.constructor.floatingPointPart(number) * newDenominator
    this.numerator = Math.floor(this.numerator)
    this.denominator = newDenominator

    if(this.numerator === 0) this.numerator = 1

    this.simplify()
  }
}

export default Fraction
