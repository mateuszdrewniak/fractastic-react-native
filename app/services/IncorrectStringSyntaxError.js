class IncorrectStringSyntaxError extends Error {
  constructor(message = "Incorrect String Syntax!") {
    super(message)
  }
}

export default IncorrectStringSyntaxError
