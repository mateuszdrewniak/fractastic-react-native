import React from 'react'
import { ImageBackground, Text, StyleSheet, View, ActivityIndicator } from 'react-native'
import mainStyles from '../styles/mainStyles'
import colors from '../styles/colors'
import Container from '../components/Container'

const splash = require('../assets/splash.png')

function AppLoadingScreen(props) {
  return (
    <Container>
      <ImageBackground source={splash} style={{width: '100%', height: '100%'}}>
        <View style={styles.loadingContainer}>
          <View>
            <Text style={[mainStyles.textWhite, mainStyles.textMd, mainStyles.textCenter]}>
              Loading...
            </Text>
            <ActivityIndicator size="large" color={colors.blue} style={styles.loadingIndicator}/>
          </View>
        </View>
      </ImageBackground>
    </Container>
  )
}

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  loadingIndicator: {
    padding: 15,
  },
})

export default AppLoadingScreen