import React, { Component } from 'react'
import { Text, TextInput, View, StyleSheet, Pressable } from 'react-native'
import { faBackspace, faDivide, faEquals, faPlus, faMinus, faTimes, faSuperscript, faHistory } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import * as Haptics from 'expo-haptics'

import mainStyles from '../styles/mainStyles'
import colors from '../styles/colors'

import Container from '../components/Container'
import CalculatorBtn from '../components/CalculatorBtn'
import CalculatorBtnRow from '../components/CalculatorBtnRow'

import Calculator from '../services/Calculator'

class CalculatorScreen extends Component {
  constructor(props) {
    super(props)
    this.state = { decimalOn: false, historyPresent: false, formula: '(0)', lastFormula: '', }
    this.canUpdateSelection = false

    this.switchDecimal = this.switchDecimal.bind(this)
    this.switchHistory = this.switchHistory.bind(this)
    this.onCalculatorBtnPress = this.onCalculatorBtnPress.bind(this)
    this.onChangeTextInput = this.onChangeTextInput.bind(this)
    this.setCanUpdateSelection = this.setCanUpdateSelection.bind(this)
    this.setCannotUpdateSelection = this.setCannotUpdateSelection.bind(this)
  }

  switchDecimal() {
    if(!this.state.decimalOn) {
      this.setState({ decimalOn: true })
      return
    }

    this.setState({ decimalOn: false })
  }

  switchHistory() {
    if(!this.state.historyPresent) {
      this.setState({ historyPresent: true })
      return
    }

    this.setState({ historyPresent: false })
  }

  setCanUpdateSelection() {
    console.log('can')
    this.canUpdateSelection = true
  }

  setCannotUpdateSelection() {
    console.log('cannot')
    this.canUpdateSelection = false
  }

  render() {
    return (
      <Container>
        <View style={styles.topBar}>
          <Pressable onPress={ this.switchHistory }>
            <View style={[styles.topBarElement]} >
              <FontAwesomeIcon size={24} icon={faHistory} style={[mainStyles.textSm, this.state.historyPresent ? mainStyles.textBlue : mainStyles.textBlack]}/>
            </View>
          </Pressable>
          <Pressable onPress={ this.switchDecimal } android_ripple={{ radius: 30 }}>
            <View style={[styles.topBarElement, styles.decimalBtn, this.state.decimalOn ? mainStyles.bgBlue : {}]}>
              <Text style={[mainStyles.textSm, mainStyles.textWhite]}>Decimal</Text>
            </View>
          </Pressable>
        </View>
        <View style={[styles.calculatorDisplay, mainStyles.bgBlack]}>
          <Text selectable={true} numberOfLines={1} ellipsizeMode={'clip'} style={[mainStyles.textMd, mainStyles.textWhite, styles.calculatorLastFormula]}>
            {this.state.lastFormula}
          </Text>

          <TextInput onSelectionChange={({ nativeEvent: { selection } }) => {
            if(!this.canUpdateSelection || selection.start !== selection.end || this.state.selection && selection.start === this.state.selection.start && selection.end === this.state.selection.end) return
            
            let caretDifference = 0
            if(this.state.selection) {
              // console.log(`${this.state.selection.start} ${this.state.selection.end} => ${selection.start} ${selection.end}`)
              if(this.state.selection.start < selection.start) caretDifference = -1
            }
            
            let newFormula = this.state.formula.replace(/\|/, '')
            let newSelection = { start: selection.start + caretDifference, end: selection.end + caretDifference }
            this.setState({ formula: newFormula.substr(0, newSelection.start) + '|' + newFormula.substr(newSelection.start), selection: newSelection })
          }} onFocus={this.setCanUpdateSelection} onPressOut={this.setCannotUpdateSelection} onChangeText={this.onChangeTextInput} showSoftInputOnFocus={false} numberOfLines={1} ellipsizeMode={'clip'} style={[mainStyles.textXl, mainStyles.textWhite, styles.calculatorFormula]}>
            {this.state.formula}
          </TextInput>

          <Text numberOfLines={1} ellipsizeMode={'clip'} style={[mainStyles.textMd, mainStyles.textWhite, styles.calculatorLastFormula]}>
          </Text>
        </View>
        <View style={[mainStyles.flex1, styles.keyboardWrapper]}>
          <View>
            <Text style={[mainStyles.textRed, mainStyles.textSm, styles.errorText]}>
              Incorrect syntax
            </Text>
          </View>
          <CalculatorBtnRow>
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="CE" themeColor={colors.orange} />
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="/" themeColor={colors.blue} />
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="<-" themeColor={colors.blue} icon={faBackspace} flex={2}/>
          </CalculatorBtnRow>
          <CalculatorBtnRow>
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="(" themeColor={colors.blue} />
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title=")" themeColor={colors.blue} />
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="^" icon={faSuperscript} themeColor={colors.blue} />
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="÷" icon={faDivide} themeColor={colors.blue} />
          </CalculatorBtnRow>
          <CalculatorBtnRow>
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="7" />
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="8" />
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="9" />
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="*" icon={faTimes} themeColor={colors.blue} />
          </CalculatorBtnRow>
          <CalculatorBtnRow>
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="4" />
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="5" />
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="6" />
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="-" icon={faMinus} themeColor={colors.blue} />
          </CalculatorBtnRow>
          <CalculatorBtnRow>
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="1" />
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="2" />
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="3" />
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="+" icon={faPlus} themeColor={colors.blue} />
          </CalculatorBtnRow>
          <CalculatorBtnRow>
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="." />
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="0" />
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="_" function=" " />
            <CalculatorBtn onPress={this.onCalculatorBtnPress} title="=" icon={faEquals} themeColor={colors.orange} />
          </CalculatorBtnRow>
        </View>
      </Container>
    )
  }

  onChangeTextInput(text) {
    if(this.state.formula === text) return

    this.setState({ formula: text })
  }

  printToCalculator(string) {
    let newFormula, selection, newSelection

    if(!this.state.selection || !this.state.selection.start || !this.state.selection.end) {
      newFormula = this.state.formula + string
      selection = { start: newFormula.length - 1, end: newFormula.length - 1 }
    } else {
      newFormula = this.state.formula.substr(0, this.state.selection.start) + string + this.state.formula.substr(this.state.selection.start)
      selection = this.state.selection
    }
    
    newSelection = { start: selection.start + string.length, end: selection.end + string.length }
    this.setState({ formula: newFormula, selection: newSelection })
  }

  dispatchBtnActions(btn) {
    switch(btn.title) {
      case '<-':
        if(!this.state.selection)
          return this.setState({ formula: this.state.formula.slice(0, -1) })
        
        let newFormula = this.state.formula.substr(0, this.state.selection.start).replace(/\|/, '')
        newFormula = newFormula.slice(0, -1) + '|' + this.state.formula.substr(this.state.selection.start).replace(/\|/, '')
        this.setState({ formula: newFormula })
        return
      case 'CE':
        this.setState({ lastFormula: '', formula: '' })
        return
      case '=':
        let lastFormula = this.state.formula.replace(/\|/, '')
        let result = Calculator.evaluate(lastFormula)
        if(this.state.decimalOn) result = `(${result.toFloat().toString()})`
        this.setState({ lastFormula: lastFormula, formula: result.toString() })
        return
      return
    }

    this.printToCalculator(btn.function)
  }

  onCalculatorBtnPress(btn, event) {
    Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Medium)
    
    this.dispatchBtnActions(btn)
  }
}

const styles = StyleSheet.create({
  topBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 5,
    paddingVertical: 5,
  },
  topBarElement: {
    paddingVertical: 3,
    paddingHorizontal: 8,
  },
  decimalBtn: {
    borderWidth: 1,
    borderColor: colors.blue,
    borderRadius: 6,
  },
  calculatorDisplay: {
    paddingHorizontal: 5,
    paddingVertical: 20,
  },
  calculatorLastFormula: {
    color: colors.blue,
    paddingBottom: 15,
    overflow: 'scroll',
    textAlign: 'right',
    fontFamily: 'Rationale-Alt',
  },
  calculatorFormula: {
    overflow: 'scroll',
    textAlign: 'right',
    fontFamily: 'Rationale-Alt',
  },
  errorText: {
    paddingHorizontal: 10,
    paddingTop: 5,
    textAlign: 'right',
  },
  keyboardWrapper: {
    marginBottom: 10,
  },
})

export default CalculatorScreen
