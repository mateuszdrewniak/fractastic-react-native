import React from 'react'
import { View, StyleSheet, StatusBar } from 'react-native'
import { StatusBar as ExpoStatusBar } from 'expo-status-bar'
import colors from '../styles/colors'

function Container(props) {
  return (
    <View style={styles.container}>
      <ExpoStatusBar style="auto"/>
      { props.children }
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.gray,
    paddingTop: StatusBar.currentHeight,
  },
})

export default Container