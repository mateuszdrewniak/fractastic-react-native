import React, { Component } from 'react'
import { Pressable, StyleSheet, View, Text } from 'react-native'
import mainStyles from '../styles/mainStyles'
import colors from '../styles/colors'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

class CalculatorBtn extends Component {
  constructor(props) {
    super(props)
    this.title = props.title
    this.flex = props.flex || 1
    this.textStyle = props.textStyle
    this.textColor =  colors.white
    this.icon = props.icon
    this.btnStyle = props.btnStyle
    this.themeColor = props.themeColor || colors.black
    this.function = props.function || props.title

    this.onPressMethod = this.onPressMethod.bind(this)
  }

  theme() {
    return {
      borderColor: this.themeColor,
      backgroundColor: this.themeColor,
    }
  }

  textTheme() {
    return {
      color: this.textColor,
    }
  }

  render() {
    if(this.icon) {
      return (
        <View style={[{ flex: this.flex },]}>
          <Pressable onPress={this.onPressMethod} android_ripple={{ radius: 70 }} style={[styles.button, this.theme(), this.btnStyle]}>
            <FontAwesomeIcon size={28} icon={ this.icon } style={[mainStyles.textBlack, styles.text, this.textTheme(), this.textStyle]} />
          </Pressable>
        </View>
      )
    }

    return (
      <View style={[{ flex: this.flex },]}>
        <Pressable onPress={this.onPressMethod} android_ripple={{ radius: 70 }} style={[styles.button, this.theme(), this.btnStyle]}>
          <Text style={[mainStyles.textLg, mainStyles.textBlack, styles.text, this.textTheme(), this.textStyle]}>
            {this.title}
          </Text>
        </Pressable>
      </View>
    )
  }

  onPressMethod(event) {
    if(typeof(this.props.onPress) !== 'function') return

    this.props.onPress(this, event)
  }
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 8,
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 30,
    marginHorizontal: 5,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 5,
    flex: 1,
  },
  text: {
    textAlign: 'center',
    textAlignVertical: 'center'
  },
})

export default CalculatorBtn