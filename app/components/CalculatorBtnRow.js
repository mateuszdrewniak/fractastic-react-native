import React from 'react'
import { View, StyleSheet } from 'react-native'

function Container(props) {
  return (
    <View style={styles.calculatorBtnRow}>
      { props.children }
    </View>
  )
}

const styles = StyleSheet.create({
  calculatorBtnRow: {
    flex: 1,
    paddingVertical: 5,
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
})

export default Container