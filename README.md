# Fractastic React Native

This app is a React Native common fraction calculator.
It's fully managed with Expo.

## Development

1. Install NodeJS (https://nodejs.org/en/download/package-manager/)

```sh
# macOS
brew install node
# Arch Linux
pacman -S nodejs npm
# Debian, Ubuntu
sudo apt update
sudo apt install nodejs
```

1. Install Expo

```sh
npm install --global expo-cli
```

3. Install Yarn

```sh
npm install --global yarn
```

4. Install dependencies

```sh
yarn install
```

5. Run tests

```sh
yarn test
```

6. Start development server

```sh
yarn start
```