export function assertEqual(shouldBe, is) {
  expect(is).toEqual(shouldBe)
}

export function assert(is) {
  expect(is).toEqual(true)
}

export function assertNot(is) {
  expect(is).toEqual(false)
}

export function assertNil(is) {
  expect(is).toEqual(null)
}

export function assertNull(is) {
  expect(is).toEqual(null)
}
