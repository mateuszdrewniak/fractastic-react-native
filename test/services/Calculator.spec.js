import { assertEqual, assert, assertNot, assertNull } from '../helpers'
import Calculator from '../../app/services/Calculator'

describe('Calculator', () => {
  test('evaluate', () => {
    evaluateNullTest('(-2) + (5) * (-5) + (1/2')
    evaluateNullTest('(-2.0) + (5) ** (-5) + (0.5)')
    evaluateNullTest('((-2.5) + (3.0))^(2.0.0) * (2.0) - (-0.5)')
    evaluateNullTest('(3 1:3) : (5:6)')
    evaluateNullTest('(3 1:3) ÷ (5/6)')
    evaluateNullTest('(3 1/3) / (5:6)')
    evaluateNullTest('((-2 1/2 2) + (3))^(2) * (2) - (-1/2)')
    evaluateNullTest('(-2 1/2 + (3))^(2) * (2) - (-1/2)')
    evaluateNullTest('(2)(1 1/2)')
    evaluateNullTest('(2) * (1 1/2)(3)')
    evaluateNullTest('(2) * (1 1/2)(3)')
    evaluateNullTest('(2) * (1 1/2)34')
  
    evaluateTest('(-2) + (5) * (-5) + (1/2)', '(-26 1/2)')
    evaluateTest('(9) / (3)', '(3)')
    evaluateTest('(9) : (3)', '(3)')
    evaluateTest('(9) ÷ (3)', '(3)')
    evaluateTest('(3 1/3) / (5/6)', '(4)')
    evaluateTest('(3 1/3) : (5/6)', '(4)')
    evaluateTest('(3 1/3) ÷ (5/6)', '(4)')
    evaluateTest('(-2.0) + (5) * (-5) + (0.5)', '(-26 1/2)')
    evaluateTest('((-2.5) + (3.0))^(2.0) * (2.0) - (-0.5)', '(1)')
    evaluateTest('((-2 1/2) + (3))^(2) * (2) - (-1/2)', '(1)')
    evaluateTest('(-3) + (1/2) * ((2) * (4 1/2)^(2)) - (8) * (1/2)', '(13 1/4)')
    evaluateTest('(((7/8) + (3/4)^(2))^(2))^(2)', '(4 17697/65536)')
    evaluateTest('(((0.875) + (0.75)^(2.0))^(2))^(2)', '(4 17697/65536)')
  })

  test('valid', () => {
    validTest('(-2) + (5) * (-5) + (1/2)', true)
    validTest('(9) / (3)', true)
    validTest('(3 1/3) / (5/6)', true)
    validTest('(9) : (3)', true)
    validTest('(9) ÷ (3)', true)
    validTest('(3 1/3) : (5/6)', true)
    validTest('(3 1/3) ÷ (5/6)', true)
    validTest('(-2.0) + (5) * (-5) + (0.5)', true)
    validTest('((-2.5) + (3.0))^(2.0) * (2.0) - (-0.5)', true)
    validTest('((-2 1/2) + (3))^(2) * (2) - (-1/2)', true)
    validTest('(-3) + (1/2) * ((2) * (4 1/2)^(2)) - (8) * (1/2)', true)
    validTest('(((7/8) + (3/4)^(2))^(2))^(2)', true)
    validTest('(((0.875) + (0.75)^(2.0))^(2))^(2)', true)
  
    validTest('(-2) + (5) * (-5) + (1/2', false)
    validTest('(3 1:3) : (5:6)', false)
    validTest('(3 1/3) : (5÷6)', false)
    validTest('(3 1÷3) ÷ (5÷6)', false)
    validTest('(-2.0) + (5) ** (-5) + (0.5)', false)
    validTest('((-2.5) + (3.0))^(2.0.0) * (2.0) - (-0.5)', false)
    validTest('((-2 1/2 2) + (3))^(2) * (2) - (-1/2)', false)
    validTest('(-2 1/2 + (3))^(2) * (2) - (-1/2)', false)
    validTest('-2 1/2 + (3)^(2) * (2) - (-1/2)', false)
    validTest('(2)(1 1/2)', false)
    validTest('(2)(1 1/2) + (3)', false)
    validTest('(2) * (1 1/2)(3)', false)
    validTest('(2) * (1 1/2)3434 3', false)
    validTest('(2) * (1 1/2)34', false)
    validTest('(2) * hhg(1 1/2)3434 3', false)
    validTest('(2) * (1 1/2)w', false)
    validTest('w(2) * (1 1/2)w', false)
    validTest('a(2) * (1 1/2)', false)
    validTest('gh', false)
    validTest('(nowy)', false)
  })
})

// private

function validTest(expression, shouldBeCorrect) {
  if(shouldBeCorrect)
    assert(Calculator.valid(expression))
  else
    assertNot(Calculator.valid(expression))
}

function evaluateNullTest(expression) {
  assertNull(Calculator.evaluate(expression))
}

function evaluateTest(expression, output) {
  assertEqual(output, Calculator.evaluate(expression).toString())
}
