import { assertEqual, assert, assertNot, assertNull } from '../helpers'
import Fraction from '../../app/services/Fraction'
import Operator from '../../app/services/Operator'

test('Operator', () => {
  fraction = new Fraction({ string: '(-2)' })

  operator = new Operator('+', 2)
  operator.increaseRank(2)
  assertEqual(4, operator.rank)

  operator.decreaseRank(2)
  assertEqual(2, operator.rank)

  operator.decreaseRank()
  assertEqual(1, operator.rank)
  
  operator.increaseRank()
  assertEqual(2, operator.rank)
  assert(operator.notExecuted())
  
  operator.markAsExecuted()
  assertNot(operator.notExecuted())
  assert(operator.executed)
  assertNull(operator.toLeft)
  assertNull(operator.toRight)
  
  operator.toLeft = fraction
  assertEqual(fraction, operator.toLeft)
  assertNull(operator.toRight)
  
  operator.toRight = fraction
  assertEqual(fraction, operator.toRight)
  assertEqual(fraction, operator.toLeft)
})


// private

function operator(rank) {
  return new Operator('+', rank)
}
