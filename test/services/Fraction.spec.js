import { assertEqual } from '../helpers'
import Fraction from '../../app/services/Fraction'

describe("Fraction", () => {
  describe("construction", () => {
    test('positive', () => {
      let fraction = new Fraction()
      expect(fraction.toString()).toEqual('(0)')

      constructionTest(0, 1, 1, '(1)')
      constructionTest(0, 0, 0, '(0)')
      constructionTest(0, 4, 1, '(4)')
      constructionTest(4, 1, 1, '(4)')
      constructionTest(0, 3, 4, '(3/4)')
    })

    test('negative', () => {
      constructionTest(-1, -16, -8, '(-3)')
      constructionTest(-1, -16, 8, '(3)')
      constructionTest(1, -16, -8, '(3)')
      constructionTest(-1, 16, -8, '(3)')
    })

    test('string', () => {
      stringConstructionTest('(-20)')
      stringConstructionTest('(1)')
      stringConstructionTest('(-3)')
      stringConstructionTest('(-0)', '(0)')
      stringConstructionTest('(-2 1/4)')
      stringConstructionTest('(-9/4)', '(-2 1/4)')
      stringConstructionTest('(9/4)', '(2 1/4)')
      stringConstructionTest('(1 -99/-4)', '(25 3/4)')
    })

    test('float string', () => {
      stringConstructionTest('(2.2)', '(2 1/5)', 2.2)
      stringConstructionTest('(2.5)', '(2 1/2)', 2.5)
      stringConstructionTest('(-2.2)', '(-2 1/5)', -2.2)
      stringConstructionTest('(0.0)', '(0)', 0.0)
      stringConstructionTest('(2.0)', '(2)', 2.0)
      stringConstructionTest('(1000.0)', '(1000)', 1000.0)
      stringConstructionTest('(2.1456)', '(2 91/625)', 2.1456)
    })

    test('float', () => {
      fraction = new Fraction({ numeric: 2.2 })
      expect(fraction.toString()).toEqual('(2 1/5)')
      expect(fraction.toFloat()).toEqual(2.2)
      floatConstructionTest(2.2, '(2 1/5)')
      floatConstructionTest(2.5, '(2 1/2)')
      floatConstructionTest(-2.2, '(-2 1/5)')
      floatConstructionTest(-2.5, '(-2 1/2)')
      floatConstructionTest(0.0, '(0)')
      floatConstructionTest(2.0, '(2)')
      floatConstructionTest(1000.0, '(1000)')
      floatConstructionTest(2.1456, '(2 91/625)')
    })
  })

  test('transformation', () => {
    transformationTest(1, -16, 8, '(-3)', '(-3)', '(-1/3)')
    transformationTest(-1, 3, 9, '(-1 1/3)', '(-4/3)', '(-3/4)')
    transformationTest(0, 20, 8, '(2 1/2)', '(5/2)', '(2/5)')
  })

  test('valid syntax', () => {
    expect(!Fraction.valid('(-2 -1-/-2)')).toEqual(true)
    expect(!Fraction.valid('(-2 1/2')).toEqual(true)
    expect(Fraction.valid('(-2 -1/-2)')).toEqual(true)
    expect(Fraction.valid('(-2 1/2)')).toEqual(true)

    expect(Fraction.valid('(1/2)')).toEqual(true)
    expect(Fraction.valid('(-135/20)')).toEqual(true)
    expect(Fraction.valid('(20)')).toEqual(true)
    expect(Fraction.valid('(0)')).toEqual(true)

    expect(!Fraction.valid('(2.0.0)')).toEqual(true)
    expect(!Fraction.valid('(w)')).toEqual(true)
    expect(!Fraction.valid('new_way')).toEqual(true)
    expect(!Fraction.valid('new.way')).toEqual(true)
    expect(!Fraction.valid('(2.0.0w)')).toEqual(true)
    expect(!Fraction.valid('jkhjk(2.0)')).toEqual(true)
    expect(!Fraction.valid('(2 1/2i)')).toEqual(true)
    expect(!Fraction.valid('(2.0')).toEqual(true)
    expect(!Fraction.valid('2.0)')).toEqual(true)
    expect(!Fraction.valid('2.0')).toEqual(true)
    expect(!Fraction.valid('(-2.-0)')).toEqual(true)
    expect(!Fraction.valid('(.523)')).toEqual(true)

    expect(Fraction.valid('(-2.5)')).toEqual(true)
    expect(Fraction.valid('(2.0)')).toEqual(true)
    expect(Fraction.valid('(2.5)')).toEqual(true)
    expect(Fraction.valid('(1.2)')).toEqual(true)
    expect(Fraction.valid('(0.125)')).toEqual(true)
    expect(Fraction.valid('(10231343.1243324)')).toEqual(true)
  })

  describe("Operations", () => {
    test('multiplication division', () => {
      fraction = newFraction(0, 3, 4).multiply(newFraction(1, 1, 3))
      expect(fraction.toString()).toEqual('(1)')
    
      fraction = newFraction(0, 3, 4).multiply(3)
      expect(fraction.toString()).toEqual('(2 1/4)')
    
      fraction = newFraction(0, 3, 4).multiply(3.5)
      expect(fraction.toString()).toEqual('(2 5/8)')
    
      fraction = newFraction(0, 3, 4).multiply(newFraction(-1, 1, 6))
      expect(fraction.toString()).toEqual('(-7/8)')
      fraction = fraction.multiply(newFraction(-1, 1, 6))
      expect(fraction.toString()).toEqual('(1 1/48)')
      fraction = fraction.divide(newFraction(-1, 1, 6))
      expect(fraction.toString()).toEqual('(-7/8)')
      fraction = fraction.divide(2)
      expect(fraction.toString()).toEqual('(-7/16)')
      fraction = fraction.multiply(4)
      expect(fraction.toString()).toEqual('(-1 3/4)')
    
      fraction = newFraction(-1, 1, 6).divide(newFraction(-1, 1, 6))
      expect(fraction.toString()).toEqual('(1)')
    
      fraction = newFraction(-1, 1, 6).divide(newFraction(1, 1, 6))
      expect(fraction.toString()).toEqual('(-1)')
    
      fraction = newFraction(0, 8, 9).divide(newFraction(1, 1, 3))
      expect(fraction.toString()).toEqual('(2/3)')
    })

    test('addition substraction', () => {
      fraction = newFraction(0, 6, 4).add(newFraction(0, 1, 2))
      expect(fraction.toString()).toEqual('(2)')
    
      fraction = newFraction(0, 5, 8).add(newFraction(0, 13, 16))
      expect(fraction.toString()).toEqual('(1 7/16)')
    
      fraction = newFraction(-2, 1, 4).subtract(newFraction(-1, 3, 5))
      expect(fraction.toString()).toEqual('(-13/20)')
    
      fraction = newFraction(-2, 1, 4).subtract(newFraction(1, 3, 5))
      expect(fraction.toString()).toEqual('(-3 17/20)')
    
      fraction = newFraction(-2, 1, 4).add(newFraction(-1, 3, 5))
      expect(fraction.toString()).toEqual('(-3 17/20)')
    
      fraction = newFraction(0, 0, 0).add(newFraction(-1, 3, 5))
      expect(fraction.toString()).toEqual('(-1 3/5)')
    
      fraction = newFraction(0, 0, 0).subtract(newFraction(-1, 3, 5))
      expect(fraction.toString()).toEqual('(1 3/5)')
    
      fraction = newFraction(-1, 3, 5).subtract(newFraction(0, 0, 0))
      expect(fraction.toString()).toEqual('(-1 3/5)')
    
      fraction = newFraction(-1, 3, 5).add(newFraction(0, 0, 0))
      expect(fraction.toString()).toEqual('(-1 3/5)')
    
      fraction = newFraction(-1, 3, 5).add(3)
      expect(fraction.toString()).toEqual('(1 2/5)')
    
      fraction = newFraction(0, 4, 1).add(7.5)
      expect(fraction.toString()).toEqual('(11 1/2)')
    
      fraction = newFraction(-1, 3, 5).add(newFraction(1, 3, 5))
      expect(fraction.toString()).toEqual('(0)')
    
      fraction = newFraction(1, 3, 5).subtract(newFraction(1, 3, 5))
      expect(fraction.toString()).toEqual('(0)')
    
      fraction = newFraction(0, 0, 0).subtract(newFraction(1, 3, 5))
      expect(fraction.toString()).toEqual('(-1 3/5)')
    
      fraction = newFraction(0, 4, 1).subtract(newFraction(0, 4, 1))
      expect(fraction.toString()).toEqual('(0)')
    
      fraction = newFraction(0, 4, 1).subtract(2.2)
      expect(fraction.toString()).toEqual('(1 4/5)')
    })

    test('power number', () => {
      fraction = newFraction(1, 1, 3)
      fraction = fraction.power(2)
      expect(fraction.toString()).toEqual('(1 7/9)')
    
      fraction = newFraction(1, 1, 3)
      fraction = fraction.power(2.0)
      expect(fraction.toString()).toEqual('(1 7/9)')
    
      fraction = newFraction(1, 1, 3)
      fraction = fraction.power(2.6)
      expect(fraction.toString()).toEqual('(2 5635701/50000000)')
    
      fraction = newFraction(-1, 1, 3)
      fraction = fraction.power(2.6)
      expect(fraction.toString()).toEqual('(-2 5635701/50000000)')
    
      fraction = newFraction(1, 1, 3)
      fraction = fraction.power(-1)
      expect(fraction.toString()).toEqual('(3/4)')
    
      fraction = newFraction(0, -4, 3)
      fraction = fraction.power(3)
      expect(fraction.toString()).toEqual('(-2 10/27)')
    
      fraction = newFraction(0, -4, 3)
      fraction = fraction.power(-3)
      expect(fraction.toString()).toEqual('(-27/64)')
    
      fraction = newFraction(0, -4, 3)
      fraction = fraction.power(-2)
      expect(fraction.toString()).toEqual('(9/16)')
    
      fraction = newFraction(0, -4, 1)
      fraction = fraction.power(-2)
      expect(fraction.toString()).toEqual('(1/16)')
    
      fraction = newFraction(0, -4, 1)
      fraction = fraction.power(2)
      expect(fraction.toString()).toEqual('(16)')
    
      fraction = newFraction(0, -4, 1)
      fraction = fraction.power(2.5)
      expect(fraction.toString()).toEqual('(-32)')
    })

    test('power fraction', () => {
      fraction = newFraction(1, 1, 3)
      fraction = fraction.power(newFraction(2, 1, 1))
      expect(fraction.toString()).toEqual('(1 7/9)')
    
      fraction = newFraction(1, 1, 3)
      fraction = fraction.power(newFraction(-1, 1, 1))
      expect(fraction.toString()).toEqual('(3/4)')
    
      fraction = newFraction(0, -4, 3)
      fraction = fraction.power(newFraction(3, 1, 1))
      expect(fraction.toString()).toEqual('(-2 10/27)')
    
      fraction = newFraction(0, -4, 3)
      fraction = fraction.power(newFraction(-3, 1, 1))
      expect(fraction.toString()).toEqual('(-27/64)')
    
      fraction = newFraction(0, -4, 3)
      fraction = fraction.power(newFraction(-2, 1, 1))
      expect(fraction.toString()).toEqual('(9/16)')
    
      fraction = newFraction(0, -4, 1)
      fraction = fraction.power(newFraction(-2, 1, 1))
      expect(fraction.toString()).toEqual('(1/16)')
    
      fraction = newFraction(0, -4, 1)
      fraction = fraction.power(newFraction(0, 4, 2))
      expect(fraction.toString()).toEqual('(16)')
    })
  })

  describe("Conversions", () => {
    test('to Integer', () => {
      fraction = newFraction(0, -4, 1)
      assertEqual(-4, fraction.toInteger())
    
      fraction = newFraction(0, 15, 20)
      assertEqual(0, fraction.toInteger())
    
      fraction = newFraction(1, 11, 20)
      assertEqual(1, fraction.toInteger())
    
      fraction = newFraction(21, -81, 99)
      assertEqual(-21, fraction.toInteger())
    })

    test('to Float', () => {
      fraction = newFraction(0, -4, 1)
      assertEqual(-4.0, fraction.toFloat())
    
      fraction = newFraction(0, 15, 20)
      assertEqual(0.75, fraction.toFloat())
    
      fraction = newFraction(1, 11, 20)
      assertEqual(1.55, fraction.toFloat())
    
      fraction = newFraction(21, -81, 99)
      assertEqual(-21.818181818181817, fraction.toFloat())
    })
  })
})

// private

function newFraction(wholePart, numerator, denominator) {
  return new Fraction({
    wholePart: wholePart,
    numerator: numerator,
    denominator: denominator
  })
}

function floatConstructionTest(float, output) {
  fraction = new Fraction({ numeric: float })

  expect(fraction.toString()).toEqual(output)
  expect(fraction.toFloat()).toEqual(float)
}

function stringConstructionTest(input, output = null, float = false) {
  if(output === null) output = input

  fraction = new Fraction({ string: input })

  expect(fraction.toString()).toEqual(output)
  expect(new Fraction({ string: fraction.toString() }).toString()).toEqual(fraction.toString())
  if(float) expect(fraction.toFloat()).toEqual(float)
}

function constructionTest(wholePart, numerator, denominator, output) {
  fraction = newFraction(wholePart, numerator, denominator)
  expect(fraction.toString()).toEqual(output)
}

function transformationTest(wholePart, numerator, denominator, output, improper, reciprocal) {
  fraction = newFraction(wholePart, numerator, denominator)

  expect(fraction.toString()).toEqual(output)
  expect(fraction.toString(true)).toEqual(improper)
  fraction.simplify()
  expect(fraction.toString()).toEqual(output)
  fraction.toReciprocal()
  expect(fraction.toString()).toEqual(reciprocal)
}
