import { assertEqual } from '../helpers'
import Operator from '../../app/services/Operator'
import OperatorArray from '../../app/services/OperatorArray'

test('OperatorArray', () => {
  operatorArray = new OperatorArray()
  operator = newOperator('+', 0)
  operatorArray.push(operator)
  operator = newOperator('-', 0)
  operatorArray.push(operator)
  operator = newOperator('*', 2)
  operatorArray.push(operator)
  operator = newOperator('+', 3)
  operatorArray.push(operator)
  operator = newOperator('^', 2)
  operatorArray.push(operator)

  operatorArray.sortDescending()
  assertEqual('(+, 3), (*, 2), (^, 2), (+, 0), (-, 0)', operatorArray.toString())

  operatorArray.sortAscending()
  assertEqual('(+, 0), (-, 0), (*, 2), (^, 2), (+, 3)', operatorArray.toString())
})


// private

function newOperator(char, rank) {
  return new Operator(char, rank)
}
