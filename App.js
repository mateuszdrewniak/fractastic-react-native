import React from 'react'
import { useFonts } from 'expo-font'
import CalculatorScreen from './app/screens/CalculatorScreen'
import AppLoadingScreen from './app/screens/AppLoadingScreen'

export default function App() {
  let [fontsLoaded] = useFonts({
    'Rationale-Alt': require('./app/assets/fonts/Rationale-Alt.ttf'),
  })

  if (!fontsLoaded) {
    return <AppLoadingScreen/>
  }


  return (
    <CalculatorScreen/>
  )
}
